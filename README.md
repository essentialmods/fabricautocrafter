# FabricAutoCrafter
## A Fabric mod that adds a new Autocrafter block

This mod adds a new block, the AutoCrafter. It allows you to put items in the crafting table and get an output. Hoppers and droppers do interact with the table.

Some of the code belongs to Gnembon. We were going to fork it, but the changes in this mod required a complete restructure of the mod.

Features that differ from Gnembon's mod:
- We don't overwrite the crafting table
- Uses Polymer to add a new Crafting Table
- Fixes a item deletion issue
- Fixes a recipe book issue
- Fixes item filters not workingd
- Adds a recipe for the Auto Crafter
